import { showModal } from './modal'
import { createFighterImage } from '../fighterPreview'

export function showWinnerModal(fighter) {
  // call showModal function 
  const fighterImage = createFighterImage(fighter)
  showModal({title:`Winner: ${fighter.name}`, bodyElement: fighterImage})
}
