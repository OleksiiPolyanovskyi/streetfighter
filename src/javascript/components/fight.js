import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  let leftFighterIndicator = document.getElementById('left-fighter-indicator');
  let rightFighterIndicator = document.getElementById('right-fighter-indicator');  

  const state = {
    first: {
      fighterInDefence: false,
      fighterHealth: firstFighter.health,
      fighterCritical: new Set(),
      fighterCriticalAllowed: true,
      fighterIndicatorWidth: 100
    },
    second: {
      fighterInDefence: false,
      fighterHealth: secondFighter.health,
      fighterCritical: new Set(),
      fighterCriticalAllowed: true,
      fighterIndicatorWidth: 100
    }    
  };  

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over    
    document.addEventListener('keydown', event => {
      let damage
      switch(event.code){
        case controls.PlayerOneAttack:
          console.log('in def',state.first.fighterInDefence);
          damage = getDamage(state.first.fighterInDefence? null : firstFighter, secondFighter, state.second.fighterInDefence);
          state.second.fighterHealth = state.second.fighterHealth - damage;
          console.log(damage);
          if (damage > 0) {
            state.second.fighterIndicatorWidth = state.second.fighterIndicatorWidth - (damage * 100)/secondFighter.health;
            rightFighterIndicator.style.width =  `${state.second.fighterIndicatorWidth}%`;
          }          
          if (state.second.fighterHealth < 0) resolve(firstFighter);
          break;
        case controls.PlayerTwoAttack:
          damage = getDamage(state.second.fighterInDefence? null : secondFighter, firstFighter, state.first.fighterInDefence);
          state.first.fighterHealth = state.first.fighterHealth - damage;
          if (damage > 0) {
            state.first.fighterIndicatorWidth = state.first.fighterIndicatorWidth - (damage * 100)/firstFighter.health;
            leftFighterIndicator.style.width =  `${state.first.fighterIndicatorWidth}%`;
          }         
          if (state.first.fighterHealth < 0) resolve(secondFighter);
          break;
        case controls.PlayerOneBlock:
          state.first.fighterInDefence = true;             
          break;
        case controls.PlayerTwoBlock:
          state.second.fighterInDefence = true;         
          break;
        case 'KeyQ':
          state.first.fighterCritical.add('KeyQ');         
          if (state.first.fighterCriticalAllowed) changeStateForCriticalHit(firstFighter, secondFighter, state, 'first', 'second', rightFighterIndicator) 
          if (state.second.fighterHealth < 0) resolve(firstFighter);
          break;
        case 'KeyW':
          state.first.fighterCritical.add('KeyW');          
          if (state.first.fighterCriticalAllowed) changeStateForCriticalHit(firstFighter, secondFighter, state, 'first', 'second', rightFighterIndicator) 
          if (state.second.fighterHealth < 0) resolve(firstFighter);
          break;
        case 'KeyE':
          state.first.fighterCritical.add('KeyE');
          if (state.first.fighterCriticalAllowed) changeStateForCriticalHit(firstFighter, secondFighter, state, 'first', 'second', rightFighterIndicator)             
          if (state.second.fighterHealth < 0) resolve(firstFighter);
          break;
        case 'KeyU':
          state.second.fighterCritical.add('KeyU');
          if (state.second.fighterCriticalAllowed) changeStateForCriticalHit(secondFighter, firstFighter, state, 'second', 'first', leftFighterIndicator)
          if (state.first.fighterHealth < 0) resolve(firstFighter);  
          break;
        case 'KeyI':
          state.second.fighterCritical.add('KeyI');
          if (state.second.fighterCriticalAllowed) changeStateForCriticalHit(secondFighter, firstFighter, state, 'second', 'first', leftFighterIndicator)
          if (state.first.fighterHealth < 0) resolve(firstFighter);   
          break;
        case 'KeyO':
          state.second.fighterCritical.add('KeyO');
          if (state.second.fighterCriticalAllowed) changeStateForCriticalHit(secondFighter, firstFighter, state, 'second', 'first', leftFighterIndicator)
          if (state.first.fighterHealth < 0) resolve(firstFighter);   
          break;
      }
    })
    document.addEventListener('keyup', event => {       
      switch(event.code){        
        case controls.PlayerOneBlock:
          state.first.fighterInDefence = false;         
          break;
        case controls.PlayerTwoBlock:
          state.second.fighterInDefence = false;         
          break;
        case 'KeyQ':
          state.first.fighterCritical.delete('KeyQ');          
          break;
        case 'KeyW':
          state.first.fighterCritical.delete('KeyW');         
          break;
        case 'KeyE':
          state.first.fighterCritical.delete('KeyE');          
          break;
        case 'KeyU':
          state.second.fighterCritical.delete('KeyU');            
          break;
        case 'KeyI':
          state.second.fighterCritical.delete('KeyI');            
          break;
        case 'KeyO':
           state.second.fighterCritical.delete('KeyO');            
          break;
      }
    })
  });
}

export function getDamage(attacker, defender, fighterInDefence) {
  // return damage 
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  if (hitPower > blockPower && !fighterInDefence){
    return hitPower - blockPower;
  }else{
    return 0;
  }
}

export function getHitPower(fighter) {
  // return hit power
  if (fighter){
    const criticalHitChance = Math.random() + 1;
    const power = fighter.attack * criticalHitChance;
    return power;
  }else {
    return 0
  }
}

export function getBlockPower(fighter) {
  // return block power 
  const dodgeChance = Math.random() + 1;
  const power = fighter.defense * dodgeChance;
  return power;  
} 

 function changeStateForCriticalHit(fighterAttacker, fighterDefender, state, attacker, defender, indicator) {
    let damage = getCriticalDamage(fighterAttacker, state[attacker].fighterCritical)
    if (damage > 0) {
      state[defender].fighterHealth = state[defender].fighterHealth - damage;
      state[attacker].fighterCriticalAllowed = false;
      state[defender].fighterIndicatorWidth = state[defender].fighterIndicatorWidth - (damage * 100)/fighterDefender.health
      indicator.style.width =  `${state[defender].fighterIndicatorWidth}%`
      setTimeout(()=>{
        state[attacker].fighterCriticalAllowed = true;
        console.log('combination allowed');
      },10000)
    }             
  }

  function getCriticalDamage(fighter, combination){
    if (combination.size === 3) {
      return fighter.attack * 2;
    }else {
      return 0;
    }  
  }